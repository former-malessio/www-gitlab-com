---
title: "Sign up for GitLab training with OlinData"
author: Walter Heck
author_gitlab:
author_twitter: walterheck
guest: true
categories: tutorial
image_title: '/images/blogimages/olindata-gitlab-training.jpg'
description: "Learn everything from basic Git to setting up GitLab CI/CD."
ee_cta: false
---

*New to GitLab, or want to level up your GitLab use? Our friends over at [OlinData](https://olindata.com/en/) are running GitLab Fundamentals Training in the Netherlands this June. Walter Heck, founder and CTO, explains what to expect and how to sign up.*

<!-- more -->

One of the projects that has made a remarkable impact in open source in the past couple of years is GitLab. When they started, they were little more than an open source clone of GitHub, but in the past years they have added many features that make it a much more compelling offer with a holistic approach to development, while still remaining very dedicated to open source.

In addition to that I have always been very impressed with the amount of openness with which [Sid Sijbrandij](https://about.gitlab.com/team/#sytses) runs GitLab. At OlinData we have actually adopted several of these things, for instance the open sourcing of our employee handbook. With the holistic approach to code management GitLab has secretly become a very powerful platform with many bells and whistles.

## This is why we’re happy to announce our first GitLab training!

It will take place in The Hague, Netherlands from June 4-6. Come join us in our office in the World Trade Center for an in-person, hands-on training that will teach you not only the basics but a lot of the more advanced features of GitLab. We'll cover:

- Git basics
- Issue tracking
- Milestones
- GitLab flow
- Merging
- CI/CD
- Server administration

Head on over to [our website](https://olindata.com/en/training/gitlab-fundamentals-training) for a full syllabus, pricing, and more information. Looking forward to having you with us to learn about GitLab!

Photo by [rawpixel.com](https://unsplash.com/photos/-MKowu9sPBc?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/search/photos/workshop?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
{: .note}
